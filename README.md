# But du repo

  - Centraliser les Interfaces de chacun
  - Faire une liste des WAs utilisées
  - Permettre de mettre à jour les données de MDT




### Plugins

Les addons utilisés par la guilde .

| Addons | Liens |
| ------ | ------ |
| Elvui | https://www.tukui.org/download.php?ui=elvui |
| WeakAura2 | https://www.curseforge.com/wow/addons/weakauras-2 |
| MDT | https://www.curseforge.com/wow/addons/mythic-dungeon-tools |



### Todos

 - Recuperer les profiles Elvui des membres de la guilde
 - Ajouter les WAs que l'on utilise


